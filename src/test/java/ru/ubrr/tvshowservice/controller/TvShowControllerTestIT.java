package ru.ubrr.tvshowservice.controller;

import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.testng.annotations.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import ru.ubrr.tvshowservice.TvShowServiceApplication;
import ru.ubrr.tvshowservice.domain.TvShow;
import ru.ubrr.tvshowservice.repositories.TvShowRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = TvShowServiceApplication.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TvShowControllerTestIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TvShowRepository tvShowRepository;

    @Test
    public void grabTvShows() throws Exception {
        mockMvc.perform(post("/tvshow/grab"))
        .andDo(print())
        .andExpect(status().isOk());
    }

    @Test
    public void find() throws Exception {
        TvShow tvShow = new TvShow().setOriginalTitle("Tv show").setRussianTitle("Сериал").setIssueDatePeriod("1990").setGenre("Драма");
        tvShowRepository.save(tvShow);

        mockMvc.perform(post("/tvshow/find")
            .param("title","show")
            .param("start","0")
            .param("length","10")
            .param("sortField","russianTitle")
            .param("sortDir","asc")
        )
            .andDo(print())
            .andExpect(status().isOk());
    }
}
