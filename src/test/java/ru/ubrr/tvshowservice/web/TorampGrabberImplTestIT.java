package ru.ubrr.tvshowservice.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import ru.ubrr.tvshowservice.TvShowServiceApplication;
import ru.ubrr.tvshowservice.domain.TvShow;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Test
@SpringBootTest(classes = TvShowServiceApplication.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TorampGrabberImplTestIT extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private TorampGrabberImpl torampGrabber;

    @Test
    public void getTvShowWithValidElement() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method method = TorampGrabberImpl.class.getDeclaredMethod("getTvShow", Element.class);
        method.setAccessible(true);
        final Element element =  Jsoup.parse("<tr>\n"
            + "    <td width=\"12%\"></td>\n"
            + "    <td width=\"88%\">\n"
            + "        <a href=\"schedule.php?id=1\" class=\"title\" title=\"Доктор Хаус (House M.D.)\">Доктор Хаус</a>&nbsp;<span\n"
            + "            class=\"year\">(2004 – 2012)</span><br>\n"
            + "        <span class=\"original-headline\">House M.D.</span><br clear=\"all\">\n"
            + "        <div class=\"ser-info\">\n"
            + "            <div><em>Сезонов:</em> <a href=\"?id=1#s8\">8</a></div>\n"
            + "            <div><em>Статус:</em> Завершен/закрыт</div>\n"
            + "            <div><em>Жанр:</em> Драма</div>\n"
            + "        </div>\n"
            + "    </td>\n"
            + "</tr>", "", Parser.xmlParser());

        final TvShow tvShow = (TvShow) method.invoke(torampGrabber, element);
    }
    @Test(expectedExceptions = InvocationTargetException.class)
    public void getTvShowWithInvalidElement() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method method = TorampGrabberImpl.class.getDeclaredMethod("getTvShow", Element.class);
        method.setAccessible(true);
        final Element element =  Jsoup.parse("<tr>\n"
            + "    <td width=\"12%\"></td>\n"
            + "    <td width=\"88%\">\n"
            + "        <div class=\"ser-info\">\n"
            + "            <div><em>Сезонов:</em> <a href=\"?id=1#s8\">8</a></div>\n"
            + "            <div><em>Статус:</em> Завершен/закрыт</div>\n"
            + "            <div><em>Жанр:</em> Драма</div>\n"
            + "        </div>\n"
            + "    </td>\n"
            + "</tr>", "", Parser.xmlParser());

        final TvShow tvShow = (TvShow) method.invoke(torampGrabber, element);
    }

}
