package ru.ubrr.tvshowservice.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;
import ru.ubrr.tvshowservice.TvShowServiceApplication;
import ru.ubrr.tvshowservice.domain.TvShow;
import ru.ubrr.tvshowservice.services.TvShowService;

import java.util.Collection;

import static org.testng.Assert.assertEquals;

@SpringBootTest(classes = TvShowServiceApplication.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TvShTvShowService extends AbstractTransactionalTestNGSpringContextTests {
    @Autowired
    TvShowService tvShowService;

    @Test
    public void findByAnyTitleContainingOneTitleMatch() throws Exception {
        TvShow tvShow =
            new TvShow().setOriginalTitle("Some tv show 1").setRussianTitle("Какой-то сериал 1").setIssueDatePeriod("1990").setGenre("Драма");
        tvShowService.save(tvShow);

        Collection<TvShow> show = tvShowService.findByAnyTitleContaining("show").getContent();
        assertEquals(show.size(), 1);
        show = tvShowService.findByAnyTitleContaining("сериал").getContent();
        assertEquals(show.size(), 1);
    }

    @Test
    public void findByAnyTitleContainingBothTitlesMatch() throws Exception {
        TvShow tvShow =
            new TvShow().setOriginalTitle("Some tv show").setRussianTitle("Какой-то сериал (Some tv show)").setIssueDatePeriod("1990").setGenre("Драма");
        tvShowService.save(tvShow);
        Collection<TvShow> show = tvShowService.findByAnyTitleContaining("show").getContent();
        assertEquals(show.size(), 1);
        show = tvShowService.findByAnyTitleContaining("сериал").getContent();
        assertEquals(show.size(), 1);
    }

    @Test
    public void findByAnyTitleContainingForSe() throws Exception {
        TvShow tvShow1 =
            new TvShow().setOriginalTitle("Some tv show 1").setRussianTitle("Какой-то сериал 1)").setIssueDatePeriod("1990").setGenre("Драма");
        tvShowService.save(tvShow1);
        TvShow tvShow2 =
            new TvShow().setOriginalTitle("Some tv show 2").setRussianTitle("Какой-то сериал 2)").setIssueDatePeriod("1990").setGenre("Драма");
        tvShowService.save(tvShow2);

        Collection<TvShow> show = tvShowService.findByAnyTitleContaining("show").getContent();
        assertEquals(show.size(), 2);
        show = tvShowService.findByAnyTitleContaining("сериал").getContent();
        assertEquals(show.size(), 2);
    }

}
