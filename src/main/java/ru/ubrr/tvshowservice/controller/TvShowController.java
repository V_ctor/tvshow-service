package ru.ubrr.tvshowservice.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ubrr.tvshowservice.domain.TvShow;
import ru.ubrr.tvshowservice.services.TvShowService;
import ru.ubrr.tvshowservice.web.WebGrabber;

import java.io.IOException;

@RestController
@RequestMapping("/tvshow")
public class TvShowController {
    private TvShowService tvShowService;
    private WebGrabber webGrabber;
    private Logger logger = LogManager.getLogger(TvShowController.class);

    @Autowired
    public TvShowController(TvShowService tvShowService, WebGrabber webGrabber) {
        this.tvShowService = tvShowService;
        this.webGrabber = webGrabber;
    }

    @PostMapping("/grab")
    public ResponseEntity<String> grabTvShows() {
        int result;
        try {
            result = webGrabber.grab();
        } catch (IOException | NullPointerException e) {
            logger.info(e);
            return new ResponseEntity<>("An error occurred: " + e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Database was updated successfully. " + result + " records were imported.", HttpStatus.OK);
    }

    @PostMapping("/find")
    public Page<TvShow> find(@RequestParam("title") String title, @RequestParam int start, @RequestParam int length,
        @RequestParam String sortField, @RequestParam String sortDir) {
        return tvShowService.findByAnyTitleContaining(title, start, length, sortField, sortDir);
    }
}
