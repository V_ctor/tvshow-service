package ru.ubrr.tvshowservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class TvShowServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvShowServiceApplication.class, args);
	}
}
