package ru.ubrr.tvshowservice.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.ubrr.tvshowservice.domain.TvShow;

public interface TvShowRepository extends JpaRepository<TvShow, Long> {
    @Query("select ts from #{#entityName} ts"
        + "  where lower(ts.originalTitle) LIKE lower(concat('%',?1,'%'))"
        + "     or lower(ts.russianTitle) LIKE lower(concat('%',?1,'%'))"
    )
    Page<TvShow> findByAnyTitleContaining(String nameSubString, Pageable pageable);
}
