package ru.ubrr.tvshowservice.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity @Data @Accessors(chain = true)
public class TvShow {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, unique = true)
    private long id;
    @Column(nullable = false)
    private String russianTitle;
    @Column(nullable = false)
    private String originalTitle;

    @Column(nullable = false)
    private String issueDatePeriod;
    @Column(nullable = false)
    private long numberOfSeasons;

    private String genre;
}
