package ru.ubrr.tvshowservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.ubrr.tvshowservice.domain.TvShow;
import ru.ubrr.tvshowservice.repositories.TvShowRepository;

@Service
public class TvShowService {
    private static final int MAX_RETURN_SIZE = 1000;
    private TvShowRepository tvShowRepository;

    @Autowired
    public TvShowService(TvShowRepository tvShowRepository) {
        this.tvShowRepository = tvShowRepository;
    }

    public Page<TvShow> findByAnyTitleContaining(String nameSubString) {
        return findByAnyTitleContaining(nameSubString, 0, MAX_RETURN_SIZE);
    }

    public Page<TvShow> findByAnyTitleContaining(String nameSubString, int start, int length) {
        assert start >= 0 && start <= MAX_RETURN_SIZE;
        assert length <= MAX_RETURN_SIZE;
        assert start < length;
        final int page = start / length;
        final Pageable pageable = new PageRequest(page, length);
        return tvShowRepository.findByAnyTitleContaining(nameSubString, pageable);
    }

    public Page<TvShow> findByAnyTitleContaining(String nameSubString, int start, int length, String sortField, String sortDir) {
        assert start >= 0 && start <= MAX_RETURN_SIZE;
        assert length <= MAX_RETURN_SIZE;
        assert start < length;
        final int page = start / length;

        final Pageable pageable = new PageRequest(page, length, Sort.Direction.fromString(sortDir), sortField);
        return tvShowRepository.findByAnyTitleContaining(nameSubString, pageable);
    }

    public void save(TvShow tvShow) {
        tvShowRepository.save(tvShow);
    }

}
