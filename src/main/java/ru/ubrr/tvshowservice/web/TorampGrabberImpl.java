package ru.ubrr.tvshowservice.web;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ubrr.tvshowservice.domain.TvShow;
import ru.ubrr.tvshowservice.repositories.TvShowRepository;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

@Component
public class TorampGrabberImpl implements WebGrabber {
    private static final String BASE_ADDRESS = "http://www.toramp.com";
    private static final String TV_SHOW_LIST_PAGE = "/schedule.php?page=";
    private TvShowRepository tvShowRepository;

    private Logger logger = LogManager.getLogger(TorampGrabberImpl.class);

    @Autowired
    public TorampGrabberImpl(TvShowRepository tvShowRepository) {
        this.tvShowRepository = tvShowRepository;
    }

    @Override public int grab() throws IOException {
        tvShowRepository.deleteAll();

        final int pagesQuantity = getPagesQuantity();
        logger.info("Total pages " + pagesQuantity);
        return parse(pagesQuantity);
    }

    private int getPagesQuantity() throws IOException {
        return Jsoup.connect(BASE_ADDRESS + TV_SHOW_LIST_PAGE).get().getElementsByClass("schedule-ddd").get(0).getElementsByTag("a").size();
    }

    private int parse(int pagesQuantity) {
        final AtomicInteger counter = new AtomicInteger(0);
        IntStream.range(0, pagesQuantity)
            .parallel() //with parallel should be careful https://dzone.com/articles/think-twice-using-java-8
            .forEach(i -> {
                    try {
                        getRow(i).forEach(row -> {
                            TvShow tvShow = getTvShow(row);
                            tvShowRepository.save(tvShow);
                            logger.info("Item " + counter.getAndIncrement() + " " + tvShow);
                        });
                    } catch (IOException e) {
                        logger.info(e);
                    }
                }
            );
        logger.info(counter + " items have been imported");
        return counter.get();
    }

    private Elements getRow(int i) throws IOException {
        return getPage(i).getElementById("schedule-list").getElementsByTag("tr");
    }

    private Document getPage(int i) throws IOException {
        return Jsoup.connect(BASE_ADDRESS + TV_SHOW_LIST_PAGE + i).get();
    }

    private TvShow getTvShow(Element row) {
        final Element element = row.getElementsByTag("td").get(1);
        final String russianTitle = element.getElementsByTag("a").get(0).text();
        final String dateString = element.getElementsByTag("span").get(0).text();
        final String originalTitle = element.getElementsByTag("span").get(1).text();
        final int seasons = Integer.parseInt(element.select("div div a").get(0).text());
        final String genre = element.select("div div:eq(2)").get(0).ownText();

        return new TvShow()
            .setRussianTitle(russianTitle)
            .setOriginalTitle(originalTitle)
            .setNumberOfSeasons(seasons)
            .setGenre(genre)
            .setIssueDatePeriod(dateString);
    }
}
