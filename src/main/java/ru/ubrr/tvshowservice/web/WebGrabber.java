package ru.ubrr.tvshowservice.web;

import java.io.IOException;

public interface WebGrabber {
    int grab() throws IOException;
}
